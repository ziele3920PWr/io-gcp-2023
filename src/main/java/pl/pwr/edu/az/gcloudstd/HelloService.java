package pl.pwr.edu.az.gcloudstd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HelloService {

    @Autowired
    private HelloRepository helloRepository;

    public void saveHello(Hello hello) {
        helloRepository.save(hello);
    }

    public Hello getHello() {
        return helloRepository.findAll().get(0);
    }
}
