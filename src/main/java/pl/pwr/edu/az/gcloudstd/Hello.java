package pl.pwr.edu.az.gcloudstd;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Hello {

    @Getter @Setter
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Getter @Setter
    private String message;
}
