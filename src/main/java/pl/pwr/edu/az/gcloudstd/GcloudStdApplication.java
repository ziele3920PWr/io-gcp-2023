package pl.pwr.edu.az.gcloudstd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GcloudStdApplication {

	public static void main(String[] args) {
		SpringApplication.run(GcloudStdApplication.class, args);
	}

}
