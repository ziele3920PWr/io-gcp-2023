package pl.pwr.edu.az.gcloudstd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class DbMockData {

    @Autowired
    private HelloService helloService;

    @EventListener(ApplicationReadyEvent.class)
    public void saveHello() {
        helloService.saveHello(new Hello(null, "Hello World from DB!"));
    }
    
}
