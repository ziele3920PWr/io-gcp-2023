package pl.pwr.edu.az.gcloudstd;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HelloRepository extends JpaRepository<Hello, Long> {
    // List<Hello> findAll();
}
